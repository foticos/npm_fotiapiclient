"use strict"
var uuid    = require('node-uuid');
var sha1    = require('sha1');
var request = require('request');
var qs      = require('qs');
var moment  = require('moment');

const VERSION = '1.01';
const PATH_AUTH = 'auth';
const DEFAULT_API_URL = 'http://api.foticos.com:8080/v2/'; // https:
//const DEFAULT_API_URL = 'https://api.foticos.com:8443/v2/

const USERAGENT = 'NODE FotiApiClient 1.09';
const TIMEOUT = 60;

let appId;
let secretKey;
let accessToken;
let publicToken;
let apiUrl;
let session;

class FotiApiClientBase {
  constructor(config, callback) {
  	this.instance = this;
    this.configBase(config);
    this.generateNewToken(function(promise){
      callback(promise);
    });
  }

  set setAppId(value) {
    this.appId = value;
  }

  set setSecretKey(value) {
    this.secretKey = value;
  }

  set setAccessToken(value) {
    this.accessToken = value;
  }

  set setPublicToken(value) {
    this.publicToken = value;
  }

  set setApiUrl(value) {
    this.apiUrl = value;
  }

  get getAppId() {
    return appId;
  }

  get getSecretKey() {
    return secretKey;
  }

  get getAccessToken() {
    return accessToken;
  }

  get getPublicToken() {
    return publicToken;
  }

  get getApiUrl() {
    return apiUrl;
  }

  configBase(config) {
    this.appId = config.appId;
    this.secretKey = config.secretKey;
    this.apiUrl = config.apiUrl;
    this.session = config.session;
  }



  static getInstance(callback){
    var  test = "pass"
    if(apiInstance == null){
      apiInstance = new FotiApiClientBase({
        appId : 77,
        secretKey : 'JIFeJIFewl83jd2Fnwl83jd2Fnee',
        apiUrl : 'http://api.foticos.pre:8080/v2/'
      }, function(promise){
        promise.then(function(flag){
          if(flag){
              //return apiInstance
              callback(apiInstance)
          }
        }).catch(function(err){
            console.log("err promise instance", err)
        })
      })

    }

  }

  generateNewToken(callback) {
    this.setAccessToken = null;
    this.setPublicToken = null;
    var _this = this;

    let timestamp = moment().unix();
    let uniqId = uuid.v1();
    let stringToHash = this.appId + "-" + this.secretKey + "-" + uniqId + "-" + timestamp;
    let hash = sha1(stringToHash);


    let fields = {
      'application_id': this.appId,
      'unique_id': uniqId,
      'timestamp': timestamp,
      'hash': hash
    }

    let output = this.makeApiRequest(PATH_AUTH, 'POST', fields, [], function(err, res){
        console.log("RES API TOKEN: ", res);
        var promiseinit = new Promise(function(resolve, reject){
          if(!err){
            this.accessToken = res.access_token;
            this.publicToken = res.public_token;
            resolve(true)
          }else{
            reject(err)
          }
        }.bind(this))

        callback(promiseinit);
    }.bind(this));

    return null;
  }

  makeApiRequest(path, method, parameters, files, callback) {
    method = typeof method !== 'undefined' ? method : 'GET';
    parameters = typeof parameters !== 'undefined' ? parameters : {};
    files = typeof files !== 'undefined' ? files : [];
    var stringPost = '';

    if (this.accessToken && !parameters.hasOwnProperty('access_token')) {
      parameters['access_token'] = this.accessToken;
    }

    if (this.accessToken && !parameters.hasOwnProperty('ext_session_id')) {
      parameters['ext_session_id'] = this.session;
    }

    if (typeof parameters == 'object' && Object.keys(parameters).length > 0 && method != 'POST') {
      console.log("PARAMETERS TEST", parameters)
      path += (((path.indexOf('?')) === false) ? '&' : '?') + qs.stringify(parameters);
    }

    if (method == 'POST')
    {
    	if (Object.prototype.toString.call(files) === '[object Array]' && files.length > 0) {
        parameters['files'] = files;
      } else {
        stringPost = '';
        if (Object.keys(parameters).length > 0) {
          for (var value in parameters) {
            if (parameters.hasOwnProperty(value)) {
              if (value == null) {
                parameters[value] = ''
              }
            }
          }
          stringPost = '?' + qs.stringify(parameters)
        }
      }
    }

    var options = {
      url: this.apiUrl + path,
      form: parameters,
      method: method,
      headers: {
        'User-Agent': USERAGENT,
        'timeout': TIMEOUT
      }
    };


    console.log("URL", options.url);
    request(options, function (error, response, body) {
      if(!error){
        callback(null, JSON.parse(body))
      }else{
        callback(error, null)
      }
    })

  }
}

module.exports = FotiApiClientBase
